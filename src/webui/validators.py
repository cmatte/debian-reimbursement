from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


def validate_username(username):
    """Some usernames lead to problematic URLs and static file names"""
    if username.startswith("."):
        raise ValidationError(_('usernames cannot start with a "."'))
    if username in ("index.html",):
        raise ValidationError(_("This username is not available"))
