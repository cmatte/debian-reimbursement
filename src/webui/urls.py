from django.urls import path

from webui.views import IndexView, ProfileView

urlpatterns = [
    path("", IndexView.as_view()),
    path("profile", ProfileView.as_view(), name="profile"),
]
