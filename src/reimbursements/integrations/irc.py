import json
import logging
import os
import os.path
import socket
from subprocess import check_call

from django.conf import settings

log = logging.getLogger(__name__)


def binary_in_path(command: str) -> bool:
    """
    Determine whether command is an executable, installed in PATH on the system
    """

    for dir_ in os.environ.get("PATH", "/usr/bin").split(":"):
        if os.access(os.path.join(dir_, command), os.X_OK):
            return True
    return False


class IrkerClient:
    """
    Irker IRC Bot integration
    Configuration:
    IRKER = {
        "SERVER": ("localhost", 6659),
        "TIMEOUT": 5, # seconds
        "MESSAGE_TYPE": "privmsg", # or "notice"
        "TARGET": "ircs://irc.debian.org/my-channel",
    }
    """

    name = "Irker"

    @classmethod
    def detect(cls):
        return hasattr(settings, "IRKER")

    def notify(self, msg):
        server = settings.IRKER.get("SERVER", ("localhost", 6659))
        timeout = settings.IRKER.get("TIMEOUT", 5)
        method = settings.IRKER.get("MESSAGE_TYPE", "privmsg")
        data = {
            "to": settings.IRKER["TARGET"],
            method: msg,
        }
        self.send(data, server=server, timeout=timeout)

    def send(self, data, server, timeout):
        serialized = json.dumps(data).encode("ASCII")
        with socket.create_connection(server, timeout=timeout) as s:
            s.sendall(serialized)


class KGBClient:
    """
    KGB IRC Bot integration
    Configuration:
    KGB_CONFIG = "/etc/kgb-client.conf"
    """

    name = "KGB"

    @classmethod
    def detect(cls):
        return hasattr(settings, "KGB_CONFIG") and binary_in_path("kgb-client")

    def notify(self, msg):
        check_call(("kgb-client", "--conf", settings.KGB_CONFIG, "--relay-msg", msg))


class IRCNotifier:
    def __init__(self):
        self.integrations = []
        for integration in [IrkerClient, KGBClient]:
            if integration.detect():
                log.debug("Configuring %s IRC client", integration.name)
                self.integrations.append(integration())
        if not self.integrations:
            log.info("No IRC clients are configured")

    def notify(self, msg):
        for integration in self.integrations:
            try:
                integration.notify(msg)
            except Exception as ex:
                log.error("Unable to notify IRC using %s: %r", integration.name, ex)


_notifier = None


def notify(msg):
    global _notifier
    if _notifier is None:
        _notifier = IRCNotifier()
    _notifier.notify(msg)
