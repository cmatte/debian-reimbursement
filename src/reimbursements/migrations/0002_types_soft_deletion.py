# Generated by Django 4.1.7 on 2023-04-16 15:40

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("reimbursements", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="expensetype",
            name="deleted",
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="requesttype",
            name="deleted",
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
