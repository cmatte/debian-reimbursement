from csv import DictReader, DictWriter
from decimal import Decimal

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand

from historical_currencies.exchange import exchange
from historical_currencies.formatting import render_amount

from reimbursements.models import (
    Request,
    RequestHistory,
    RequestLine,
    RequestType,
    ExpenseType,
)

User = get_user_model()


COUNTRY_CURRENCY = {
    "AL": "EUR",
    "BE": "EUR",
    "BR": "BRL",
    "CA": "CAD",
    "DE": "EUR",
    "FR": "EUR",
    "GT": "GTQ",
    "IL": "ILS",
    "IN": "INR",
    "IT": "EUR",
    "KR": "KRW",
    "NG": "NGN",
    "NP": "NPR",
    "PL": "PLN",
    "TR": "TRY",
    "TW": "TWD",
    "US": "USD",
    "UY": "UYU",
    "ZA": "ZAR",
    "__": "USD",
}

COUNTRY_PAYER = {
    "AL": "Debian France",
    "BE": "Debian France",
    "BR": "SPI",
    "CA": "SPI",
    "DE": "Debian France",
    "FR": "Debian France",
    "GT": "SPI",
    "IL": "SPI",
    "IN": "SPI",
    "IT": "Debian France",
    "KR": "SPI",
    "NG": "SPI",
    "NP": "SPI",
    "PL": "Debian France",
    "TR": "SPI",
    "TW": "SPI",
    "US": "SPI",
    "UY": "SPI",
    "ZA": "SPI",
    "__": "SPI",
}


def existing_parent(arg: str) -> tuple[str, Request]:
    """Parse a --existing-parent argument"""
    budget_name, req_num = arg.split(":")
    parent = Request.objects.get(id=int(req_num))
    return (budget_name, parent)


def expense_type(arg: str) -> ExpenseType:
    """Look up an Expense Type by name"""
    try:
        return ExpenseType.objects.get(name=arg)
    except ExpenseType.DoesNotExist as e:
        raise ValueError(e)


def group_name(arg: str) -> Group:
    """Look up a Group by name"""
    try:
        return Group.objects.get_by_natural_key(arg)
    except Group.DoesNotExist as e:
        raise ValueError(e)


def pretty_budget_name(name: str) -> str:
    """Human readable budget name"""
    if name.startswith("dc-"):
        name = name.split("-", 1)[1]
        if name == "ctte":
            name = "DebConf Committee"
        else:
            name = f"DebConf {name.title()} Team"
    else:
        name = name.title()
    return name


class Command(BaseCommand):
    help = "Load a CSV file with a set of approved bursaries"

    def add_arguments(self, parser):
        parser.add_argument(
            "bursaries",
            type=open,
            metavar="CSV",
            help="List of bursaries to import into the system",
        )
        parser.add_argument(
            "--dry-run",
            action="store_true",
            help="Only print what we would do, don't perform any actions",
        )
        parser.add_argument(
            "--owner",
            metavar="USER",
            required=True,
            help="Create parent requests belonging to USER",
        )
        parser.add_argument(
            "--existing-parent",
            "-p",
            action="append",
            metavar="BUDGET:NUM",
            type=existing_parent,
            default=[],
            help="Attach requests for BUDGET to request NUM "
            "(can be specifide multiple times)",
        )
        parser.add_argument(
            "--approver-group",
            metavar="NAME",
            default="DebConf Treasurers",
            type=group_name,
            help="Make NAME the approvers for the created requests "
            "(default: DebConf Treasurers)",
        )
        parser.add_argument(
            "--payer-group",
            metavar="NAME",
            default="SPI",
            type=group_name,
            help="Make NAME the payers for the created requests " "(default: SPI)",
        )
        parser.add_argument(
            "--expense-type",
            metavar="NAME",
            default="Flight Tickets",
            type=expense_type,
            help="Create Request lines with NAME Expense Type",
        )
        parser.add_argument(
            "--event-name",
            metavar="NAME",
            default="DebConf",
            help="Name of the event",
        )

    def handle(self, *args, **options):
        self.budgets = dict(options["existing_parent"])

        r = DictReader(options["bursaries"])
        w = DictWriter(
            self.stdout,
            [
                "user.username",
                "converted_amount",
                "currency",
                "selected_to",
                "request_url",
            ],
        )
        w.writeheader()
        for row in r:
            if row["request_travel"] != "True":
                continue
            if row["user.attendee.arrived"] != "True":
                continue
            if row["travel_status"] != "accepted":
                continue
            req = self.handle_request(row, options)
            if req:
                if options["dry_run"]:
                    # We can't hit .total unless the object has been saved
                    converted_amount = Decimal(row["travel_bursary"])
                    currency = req.currency
                    converted_amount = exchange(converted_amount, "USD", currency)
                else:
                    converted_amount, currency = req.total
                converted_amount = render_amount(converted_amount, currency).split(" ")[
                    0
                ]
                w.writerow(
                    {
                        "user.username": req.requester.username,
                        "converted_amount": converted_amount,
                        "currency": currency,
                        "selected_to": req.payer_group.name,
                        "request_url": req.get_fully_qualified_url(),
                    }
                )

    def create_parent_request(
        self,
        event_name: str,
        budget: str,
        owner: User,
        approver_group: Group,
        payer_group: Group,
        dry_run: bool,
    ):
        budget_name = pretty_budget_name(budget)
        request = Request(
            requester=owner,
            parent=None,
            state=Request.States.PENDING_APPROVAL,
            request_type=RequestType.objects.get(name="Travel"),
            description=f"{event_name} {budget_name} Travel Reimbursements",
            currency="USD",
            approver_group=approver_group,
            payer_group=payer_group,
        )
        created = RequestHistory(
            request=request,
            actor=owner,
            event=RequestHistory.Events.CREATED,
        )
        self.budgets[budget] = request
        if dry_run:
            print(f"Would create parent Request: {request}")
        else:
            request.save()
            created.save()

    def create_user(
        self, username: str, email: str, first_name: str, last_name: str, dry_run: bool
    ) -> User:
        """Create and return a User with the given details"""
        args = {
            "username": username,
            "email": email,
            "password": None,
            "first_name": first_name,
            "last_name": last_name,
        }
        if dry_run:
            print(f"Would create user: {username} ({email})")
            return User(**args)

        user = User.objects.create_user(**args)
        # An unuseable password that Django will allow to be reset
        user.password = "@" + user.password
        user.save()
        return user

    def handle_request(self, row: dict, options: dict):
        dry_run = options["dry_run"]
        budget = row["budget"] or "general"
        event_name = options["event_name"]
        owner = User.objects.get(username=options["owner"])
        approver_group = options["approver_group"]
        payer_group = options["payer_group"]

        parent = None
        if budget:
            if budget not in self.budgets:
                self.create_parent_request(
                    event_name=event_name,
                    budget=budget,
                    owner=owner,
                    approver_group=approver_group,
                    payer_group=payer_group,
                    dry_run=dry_run,
                )
            parent = self.budgets[budget]

        try:
            requester = User.objects.get(email=row["user.email"])
        except User.DoesNotExist:
            requester = self.create_user(
                username=row["user.username"],
                email=row["user.email"],
                first_name=row["user.first_name"],
                last_name=row["user.last_name"],
                dry_run=dry_run,
            )

        existing = Request.objects.filter(requester=requester, parent=parent)
        if existing.exists():
            existing_req = existing.first()
            print(
                f"Found an existing request for {requester.username}: {existing_req}, skipping..."
            )
            return

        country = row["country"]
        currency = COUNTRY_CURRENCY[country]
        payer_group = group_name(COUNTRY_PAYER[country])
        requester_name = f"{row['user.first_name']} {row['user.last_name']}"

        request = Request(
            requester=requester,
            parent=parent,
            state=Request.States.APPROVED,
            request_type=RequestType.objects.get(name="Travel"),
            description=f"{event_name} Travel Reimbursements for {requester_name}",
            currency=currency,
            approver_group=approver_group,
            payer_group=payer_group,
        )
        amount = Decimal(row["travel_bursary"])
        if currency != "USD":
            amount = exchange(amount, "USD", currency)
        line = RequestLine(
            request=request,
            expense_type=options["expense_type"],
            description="Travel to DebConf",
            amount=amount,
        )
        created = RequestHistory(
            request=request,
            actor=owner,
            event=RequestHistory.Events.CREATED,
        )
        approved = RequestHistory(
            request=request,
            actor=owner,
            event=RequestHistory.Events.APPROVED,
            budget={},
            details="Imported approved bursaries",
        )
        if dry_run:
            print(
                f"Would create Request for {requester.username} in {currency} paid by {payer_group}"
            )
        else:
            request.save()
            line.save()
            created.save()
            approved.budget = request.summarize_by_type_dict()
            approved.save()
        return request
