import os

from django.conf import settings
from django.core.checks import Error, Tags, register
from django.core.files.storage import FileSystemStorage


class PrivateStorage(FileSystemStorage):
    def url(self, name):
        # FIXME: Ideally: base_url = None
        # But this breaks widget rendering.
        # So, instead we just return a useless URL.
        return "#"


def generate_receipt_filename(instance, filename):
    return settings.RECEIPTS_FILENAME.format(instance=instance, filename=filename)


receipts_storage = PrivateStorage(location=settings.RECEIPTS_PATH)


# Not the correct tag, but the options are limited...
@register(Tags.sites, deploy=True)
def check_storage_is_writeable(app_configs, **kwargs):
    errors = []
    storage_path = receipts_storage.path("")
    if not os.access(storage_path, os.W_OK):
        errors.append(
            Error(
                "receipts directory not writeable",
                hint="Make the RECEIPTS_PATH a directory that the Django user can write to",
                id="reimbursements.E001",
            )
        )

    return errors
