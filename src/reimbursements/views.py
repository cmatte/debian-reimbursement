from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import FileResponse, HttpResponseNotAllowed
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, FormView, UpdateView

from reimbursements.forms import (
    BankDetailsForm,
    ReceiptForm,
    ReceiptLineEditFormset,
    ReceiptLineFormHelper,
    ReceiptLineFormset,
    RequestForm,
    RequestLineFormHelper,
    RequestLineFormset,
    RequestReimbursementForm,
    RequestSubmitForm,
)
from reimbursements.lifecycle import (
    request_approved,
    request_created,
    request_commented,
    request_proposed,
    request_proposed_increase,
    request_reimbursed,
    request_rejected,
    request_returned_for_more_info,
    reimbursement_returned_for_more_info,
    request_submitted,
    request_updated,
    receipt_created,
    receipt_deleted,
    receipt_updated,
)
from reimbursements.models import (
    ExpenseType,
    Profile,
    Receipt,
    ReimbursementCost,
    ReimbursementLine,
    Request,
    RequestType,
)


class BankDetailsView(LoginRequiredMixin, UpdateView):
    model = Request
    template_name = "reimbursements/bank_details.html"
    context_object_name = "object"
    form_class = BankDetailsForm

    def get_initial(self):
        try:
            bank_details = self.request.user.profile.bank_details
        except Profile.DoesNotExist:
            bank_details = None
        if not bank_details:
            bank_details = {
                "recipient_name": self.request.user.get_full_name(),
                "email": self.request.user.email,
            }
        return bank_details

    def get_context_data(self, **kwargs):
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        context = super().get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        Profile.objects.update_or_create(
            user=self.request.user, defaults={"bank_details": form.cleaned_data}
        )
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.get_absolute_url()


class RequestView(LoginRequiredMixin, DetailView):
    model = Request
    template_name = "reimbursements/request.html"
    context_object_name = "object"

    def get_context_data(self, **kwargs):
        access = self.object.user_access(self.request.user)
        if not access:
            raise PermissionDenied()
        context = super().get_context_data(**kwargs)
        context["access"] = access
        return context


class FormAndSetMixin:
    """
    A Class Based View mixin for a primary form with dependent formset

    Basically, a fairly minimal thing that combines with CreateView/UpdateView
    """

    def get_formset_class(self):
        """
        Returns the formset class to use in this view
        """
        return self.formset_class

    def get_formset(self, formset_class, instance=None):
        """
        Returns an instance of the formset to be used in this view.
        Optionally takes the takes the instance of the parent object.
        """
        kwargs = self.get_form_kwargs()
        if instance:
            kwargs["instance"] = instance
        return formset_class(**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if "formset" not in context:
            context["formset"] = RequestLineFormset()
        return context

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class)
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            self.object = form.save(commit=False)
        formset_class = self.get_formset_class()
        formset = self.get_formset(formset_class, instance=self.object)
        if self.is_valid(form, formset):
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def is_valid(self, form, formset):
        return form.is_valid() and formset.is_valid()

    def form_invalid(self, form, formset):
        return self.render_to_response(
            self.get_context_data(form=form, formset=formset)
        )

    def form_valid(self, form, formset):
        with transaction.atomic():
            self.object.save()
            formset.save()
        return self.get_success_url()

    def get_success_url(self):
        return redirect(self.object.get_absolute_url())


class FormAndSetCreateView(FormAndSetMixin, CreateView):
    """A form+formset CreateView"""

    def get(self, request, *args, **kwargs):
        self.object = None
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = None
        return super().post(request, *args, **kwargs)


class FormAndSetUpdateView(FormAndSetMixin, UpdateView):
    """A form+formset UpdateView"""

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)


class DetailActionView(DetailView):
    """A DetailView for performing actions only"""

    def get(self, request, *args, **kwargs):
        # DetailView provides a get()
        return HttpResponseNotAllowed(["post"])


class RequestCreateView(LoginRequiredMixin, FormAndSetCreateView):
    template_name = "reimbursements/edit_request.html"
    form_class = RequestForm
    formset_class = RequestLineFormset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["formset_helper"] = RequestLineFormHelper()
        context["expense_type_map"] = RequestType.expense_type_map()
        return context

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.request_user = self.request.user
        return form

    def form_valid(self, form, formset):
        with transaction.atomic():
            r = super().form_valid(form, formset)
            request_created(request=self.object, actor=self.request.user)
        return r


class RequestEditView(LoginRequiredMixin, FormAndSetUpdateView):
    template_name = "reimbursements/edit_request.html"
    model = Request
    context_object_name = "object"
    form_class = RequestForm
    formset_class = RequestLineFormset

    def get_context_data(self, **kwargs):
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        context = super().get_context_data(**kwargs)
        context["formset_helper"] = RequestLineFormHelper()
        context["expense_type_map"] = RequestType.expense_type_map()
        return context

    def form_valid(self, form, formset):
        with transaction.atomic():
            r = super().form_valid(form, formset)
            request_updated(request=self.object, actor=self.request.user)
        return r


class RequestSubmitDraftView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        if self.object.state != Request.States.DRAFT:
            raise PermissionDenied("Only DRAFT requests can be submitted")
        details = request.POST.get("details")
        request_proposed(request=self.object, actor=self.request.user, details=details)
        return redirect(self.object.get_absolute_url())


class RequestCommentView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user):
            raise PermissionDenied("Can only comment on requests you have access to")
        details = request.POST.get("details")
        request_commented(request=self.object, actor=self.request.user, details=details)
        return redirect(self.object.get_absolute_url())


class RequestSubmitIncreaseView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        if self.object.state != Request.States.APPROVED:
            raise PermissionDenied(
                "Only APPROVED requests can be submitted for an increase"
            )
        details = request.POST.get("details")
        request_proposed_increase(
            request=self.object, actor=self.request.user, details=details
        )
        return redirect(reverse("request-edit", kwargs=self.kwargs))


class RequestSubmitView(LoginRequiredMixin, FormView):
    template_name = "reimbursements/submit_request.html"
    form_class = RequestSubmitForm

    def get_form(self, form_class=None):
        self.object = Request.objects.get(pk=self.kwargs["pk"])
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        if self.object.state != Request.States.APPROVED:
            raise PermissionDenied("Only APPROVED requests can be submitted")
        return super().get_form(form_class=form_class)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = self.object
        return context

    def form_valid(self, form):
        request_submitted(request=self.object, actor=self.request.user)
        return redirect(self.object.get_absolute_url())


class RequestReviewView(LoginRequiredMixin, DetailView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user).get("approver", False):
            raise PermissionDenied(
                "Can only review requests that you are an approver for"
            )
        if self.object.state != Request.States.PENDING_APPROVAL:
            raise PermissionDenied("Only PENDING_APPROVAL requests can be reviewed")
        details = request.POST.get("details")
        if request.POST.get("approve"):
            request_approved(
                request=self.object, actor=self.request.user, details=details
            )
        elif request.POST.get("reject"):
            request_rejected(
                request=self.object, actor=self.request.user, details=details
            )
        elif request.POST.get("more_info"):
            request_returned_for_more_info(
                request=self.object, actor=self.request.user, details=details
            )
        else:
            raise Exception("Unknown action")
        return redirect(self.object.get_absolute_url())

    def get(self, request, *args, **kwargs):
        # DetailView provides a get()
        return HttpResponseNotAllowed(["post"])


class RequestReimburseView(LoginRequiredMixin, FormView):
    template_name = "reimbursements/reimburse_request.html"
    form_class = RequestReimbursementForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.object = Request.objects.get(pk=self.kwargs["pk"])
        kwargs["request"] = self.object
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.object.user_access(self.request.user).get("payer", False):
            raise PermissionDenied("Only the assigned payer can edit your requests")
        if self.object.state != Request.States.PENDING_PAYMENT:
            raise PermissionDenied("Only PENDING_PAYMENT requests can be reimbursed")
        context["object"] = self.object
        return context

    def form_valid(self, form):
        with transaction.atomic():
            data = form.cleaned_data.copy()
            details = data.pop("details")
            fee = data.pop("fee")
            fee_currency = data.pop("fee_currency")
            fee_details = data.pop("fee_details")
            for k, v in data.items():
                ReimbursementLine(
                    request=self.object,
                    expense_type=ExpenseType.objects.get(id=int(k)),
                    amount=v,
                ).save()
            if fee:
                ReimbursementCost(
                    request=self.object,
                    description=fee_details,
                    amount=fee,
                    currency=fee_currency,
                ).save()
            request_reimbursed(self.object, self.request.user, details=details)
        return redirect(self.object.get_absolute_url())


class RequestReimburseReturnView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user).get("payer", False):
            raise PermissionDenied("Only the assigned payer can edit your requests")
        if self.object.state != Request.States.PENDING_PAYMENT:
            raise PermissionDenied(
                "Only PENDING_PAYMENT requests can be returned from reimbursement"
            )
        details = request.POST.get("details")
        reimbursement_returned_for_more_info(
            request=self.object, actor=self.request.user, details=details
        )
        return redirect(reverse("request", kwargs=self.kwargs))


class ReceiptChildView:
    template_name = "reimbursements/edit_receipt.html"
    form_class = ReceiptForm
    formset_class = ReceiptLineFormset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["formset_helper"] = ReceiptLineFormHelper()
        # TODO: We could limit the options in the select box, server-side. But
        # this means passing context more through to the form instance.
        context["valid_expense_types"] = [
            expense_type.id
            for expense_type in self.request_model.request_type.expense_types.all()
        ]
        return context

    def get_request(self):
        self.request_model = Request.objects.get(id=self.request_id)
        if not self.request_model.user_access(self.request.user).get(
            "requester", False
        ):
            raise PermissionDenied("Can only edit your own requests")
        if self.request_model.state != Request.States.APPROVED:
            raise PermissionDenied("Requests need to be approved to edit receipts")

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        form.request = self.request_model
        return form

    def get_formset(self, formset_class, instance=None):
        formset = super().get_formset(formset_class, instance=instance)
        formset.default_currency = self.request_model.currency
        return formset

    def get(self, request, *args, **kwargs):
        self.request_id = kwargs["request_id"]
        self.get_request()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.request_id = kwargs["request_id"]
        self.get_request()
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return redirect(self.request_model.get_absolute_url())


class ReceiptCreateView(ReceiptChildView, LoginRequiredMixin, FormAndSetCreateView):
    def is_valid(self, form, formset):
        for subform in formset.forms:
            # During validation of new object creation, we can't access the
            # receipt's request object. Smuggle it in.
            subform.instance.initial_request_currency = self.request_model.currency
        return super().is_valid(form, formset)

    def form_valid(self, form, formset):
        with transaction.atomic():
            r = super().form_valid(form, formset)
            receipt_created(receipt=self.object, actor=self.request.user)
        return r


class ReceiptEditView(ReceiptChildView, LoginRequiredMixin, FormAndSetUpdateView):
    formset_class = ReceiptLineEditFormset
    model = Receipt

    def form_valid(self, form, formset):
        with transaction.atomic():
            r = super().form_valid(form, formset)
            receipt_updated(receipt=self.object, actor=self.request.user)
        return r


class ReceiptDeleteView(LoginRequiredMixin, DetailActionView):
    model = Receipt

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        request = self.object.request
        if not request.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only delete receipts for your own requests")
        if self.object.request.state != Request.States.APPROVED:
            raise PermissionDenied("Requests need to be approved to edit receipts")
        receipt_deleted(receipt=self.object, actor=self.request.user)
        return redirect(request.get_absolute_url())


@method_decorator(xframe_options_sameorigin, name="dispatch")
class ReceiptFileView(LoginRequiredMixin, DetailView):
    model = Receipt

    def render_to_response(self, context):
        if not self.object.request.user_access(self.request.user):
            raise PermissionDenied()
        return FileResponse(self.object.file)


class RequestReceiptsView(LoginRequiredMixin, DetailView):
    model = Request
    template_name = "reimbursements/request_receipts.html"
    context_object_name = "object"

    def get_context_data(self, **kwargs):
        access = self.object.user_access(self.request.user)
        if not access:
            raise PermissionDenied()
        context = super().get_context_data(**kwargs)
        context["access"] = access
        return context
